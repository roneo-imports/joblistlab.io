# joblist.city

This is a website (frontend) for [joblist.gitlab.io](https://joblist.gitlab.io).

## Overview

- on the website, it is possible to search (algolia) an index of currently open jobs (from a list of companies currently hirring).
- the [list of companies](https://github.com/joblistcity/companies), helping to fetch the jobs is publicly available and editable (netlify-cms open-authoring)
- the [joblist/workers](https://gitlab.com/joblist/workers), clone the companies data above, fetch their related jobs (if any), and send all jobs to an Algolia search index (available to search on joblist.city)

## Deployment

- all the code is in `/index`
- all of it is moved to `/public` when deployed, which it is served by gitlab pages (index.html)
- deployed with `.gitlab-ci.yml`, `pages` job.
